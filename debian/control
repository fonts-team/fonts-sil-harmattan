Source: fonts-sil-harmattan
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Bobby de Vos <bobby_devos@sil.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://software.sil.org/harmattan
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-harmattan.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-harmattan
Rules-Requires-Root: no

Package: fonts-sil-harmattan
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Arabic script font designed for use by languages in West Africa
 Harmattan, named after the trade winds that blow during the dry season in
 West Africa, is designed in a Warsh style to suit the needs of languages
 using the Arabic script in West Africa.
 .
 This release supports virtually all of the Unicode 13.0 Arabic character
 repertoire (excluding the Quranic annotation signs in the Arabic Extended-A
 block and excluding the Arabic Presentation Forms blocks, which are not
 recommended for normal use). Font smarts are implemented using OpenType
 and Graphite technologies.
 .
 This font uses state-of-the-art OpenType and Graphite font technologies,
 including variant glyphs for a number of characters for use in particular
 contexts.
 .
 This font provides a simplified rendering of Arabic script, using basic
 connecting glyphs but not including a wide variety of additional ligatures
 or contextual alternates (only the required lam-alef ligatures). This
 simplified style is often preferred for clarity, especially in non-Arabic
 languages, but may be considered unattractive in more traditional and
 literate communities.
 .
 Four fonts from this typeface family are included in this release:
  * Harmattan Regular
  * Harmattan Medium
  * Harmattan SemiBold
  * Harmattan Bold
 .
 Webfont versions and HTML/CSS examples are also available.
 .
 The full font sources are publicly available at
 https://github.com/silnrsi/font-harmattan
 An open workflow is used for building, testing and releasing.
